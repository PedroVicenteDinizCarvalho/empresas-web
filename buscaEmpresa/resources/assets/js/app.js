
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vuex from 'Vuex';
Vue.use(Vuex);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 const store = new Vuex.Store({
 	state:{
 		itens:{teste:"opa funcionou"}
 	},
 	mutations:{
 		setItens(state,obj){
 			state.itens = obj;
 		}
 	}
 });

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('box-component', require('./components/boxes/BoxComponent.vue'));
Vue.component('page-component', require('./components/pages/PageComponent.vue'));
Vue.component('breadcrumb-component', require('./components/navs/BreadcrumbComponent.vue'));
Vue.component('card-component', require('./components/boxes/CardComponent.vue'));
Vue.component('modal-component', require('./components/modal/ModalComponent.vue'));
Vue.component('modallink-component', require('./components/modal/ModallinkComponent.vue'));
Vue.component('form-component', require('./components/form/FormComponent.vue'));

const app = new Vue({
    el: '#app',
    store
});
