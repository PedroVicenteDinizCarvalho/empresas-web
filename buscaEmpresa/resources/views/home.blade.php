@extends('layouts.app')

@section('content')
	<page-component size="12" title="home">
		<breadcrumb-component 
			v-bind:list="{{$listLinks}}">

		</breadcrumb-component>

		<div class="row">
			<box-component 
				title="Empresas" icon="ion ion-podium" color="bg-aqua" url="admin/empresas">
			</box-component>
		</div>
	</page-component>
@endsection
