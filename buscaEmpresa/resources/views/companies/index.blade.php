@extends('layouts.app')

@section('content')
	<page-component size="12" title="Empresas">
		<breadcrumb-component 
			v-bind:list="{{$listLinks}}">
		</breadcrumb-component>		
		<card-component v-bind:itens="[[1, 'Way togo', 'negócios', '111.111.1111.11', 'A empresa de tecnologia', 'Brasil'], [2, 'Matec', 'agronomia', '111.111.1111.11', 'A empresa de agronomia', 'Brasil']]" detail="#detail" token="4141312312321" edit="#edit" remove="#remove" create="#create" modal="sim"></card-component>
		
	</page-component>

	<modal-component name="create">
		<form-component id="formCreate" css="" action="#create" method="post" enctype="" token="{{ csrf_token() }}">
		  <div class="form-group">
		    <label for="exampleInputEmail1">Email address</label>
		    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
		    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Password</label>
		    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
		  </div>
		  <div class="form-check">
		    <input type="checkbox" class="form-check-input" id="exampleCheck1">
		    <label class="form-check-label" for="exampleCheck1">Check me out</label>
		  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>			
		</form-component>		
	</modal-component>

	<modal-component name="edit">
		<form-component id="formCreate" css="" action="#create" method="post" enctype="" token="{{ csrf_token() }}">
		  <div class="form-group">
		    <label for="exampleInputEmail1">Email address</label>
		    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
		    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Password</label>
		    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
		  </div>
		  <div class="form-check">
		    <input type="checkbox" class="form-check-input" id="exampleCheck1">
		    <label class="form-check-label" for="exampleCheck1">Check me out</label>
		  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>			
		</form-component>		
	</modal-component>
@endsection