<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        
        <link rel="stylesheet" href="./css/app.css">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                @if (Route::has('login'))
                    @auth
                    <div class="title m-b-md">
                        <img src="./img/logo-home.png" alt="Logo Ioasys"><br>
                        <h1 class="title">BuscaEmpresa</h1>
                    </div>
                    @else
                    <div class="title m-b-md">
                        <img src="./img/logo-home.png" alt="Logo Ioasys"><br>
                        <h1 class="title">BEM-VINDO AO <br> BuscaEmpresa</h1> 
                        <p class="subtitle">Cadastre empresas e pesquise quando e onde estiver</p>
                    </div>
                    <div class="links">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class=" panel-default">
                                        <div class="panel-body">
                                            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                                {{ csrf_field() }}

                                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <label for="email" class="col-md-4 control-label"><i class="material-icons">mail</i></label>

                                                    <div class="col-md-5">
                                                        <input id="email" type="email" class="form-login" name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail">

                                                        @if ($errors->has('email'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                    <label for="password" class="col-md-4 control-label"><i class="material-icons">lock</i></label>

                                                    <div class="col-md-5">
                                                        <input id="password" type="password" class="form-login" name="password" placeholder="Senha" required>

                                                        @if ($errors->has('password'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-6 col-md-offset-3">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-8 col-md-offset-2">
                                                        <button type="submit" class="btn btn-primary button">
                                                            Login
                                                        </button>
                                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                                            Forgot Your Password?
                                                        </a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>   
                    @endauth
                @endif
            </div>
        </div>
    </body>
</html>
